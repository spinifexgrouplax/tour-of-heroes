# Common Nginx configuration
FROM nginx:alpine as nginx-base
COPY ./docker/nginx/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 4200

# Build stage
FROM node:15-slim as build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json package-lock.json /app/
RUN npm install
COPY . /app

# ------------------------

# Build STAGING
FROM build-step as build-staging
RUN node --max_old_space_size=2048 ./node_modules/@angular/cli/bin/ng build --configuration=staging

# Release STAGING
FROM nginx-base as release-staging
COPY --from=build-staging /app/dist/tour-of-heroes /usr/share/nginx/html

# ------------------------

# Build PRODUCTION
FROM build-step as build-production
RUN node --max_old_space_size=2048 ./node_modules/@angular/cli/bin/ng build --configuration=production

# Release PRODUCTION
FROM nginx-base as release-production
COPY --from=build-production /app/dist/tour-of-heroes /usr/share/nginx/html
